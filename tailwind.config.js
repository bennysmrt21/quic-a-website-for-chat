module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {
      spacing: {
        '101': '29rem',
        '99': '22.2rem',
        '104': '30rem',
        '105': '31.4rem',
        '107': '32.7rem',

        '106': '33rem',
        '108': '35.9rem',
        '110': '37.6rem',
        '111': '33.5rem',
        '109': '39.6rem',
      }
    },
  },
  plugins: [],
}
